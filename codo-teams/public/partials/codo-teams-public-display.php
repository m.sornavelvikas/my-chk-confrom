<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       www.codosome.com
 * @since      1.0.0
 *
 * @package    Codo_Teams
 * @subpackage Codo_Teams/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
