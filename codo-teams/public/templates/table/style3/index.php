<?php
    $members_list=explode(',',$members_list);
?>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
        <?php echo $custom_css; ?>
        </style>
    </head> 
    <body>
        <div class="codo_teams codo_table style_3 <?php echo 'codoteam_'.$id; ?>">
            <div class="wrapper">
                <div class="elements_container">
                    <div class="profile-table table-style style-1">
                        <div class="profile-table-row profile-table-head">
                            <div class="profile-table-cell">
                                Image          
                            </div>
                            <!-- .profile-table-cell -->
                            <div class="profile-table-cell">
                                Name           
                            </div>
                            <!-- .profile-table-cell -->
                            <div class="profile-table-cell">
                                Designaion         
                            </div>
                            <!-- .profile-table-cell -->
                            <div class="profile-table-cell">
                                Short Description          
                            </div>
                            <!-- .profile-table-cell -->         
                            <div class="profile-table-cell">
                                Social Links           
                            </div>
                            <!-- .profile-table-cell -->
                        </div>
                        <?php 
                            foreach($members_list as $id)
                            {
                                ?>
                                    <?php $social_media=get_post_meta($id,"social_media",true); ?>
                                    <div class="profile-table-row">
                                        <div class="profile-table-cell profile-table-image">
                                            <div class="profile-table-img-holder">
                                                <img src="<?php echo get_the_post_thumbnail_url($id); ?>" alt="img01">
                                            </div>
                                            <!-- .profile-img-holder -->
                                        </div>
                                        <!-- .profile-table-cell -->
                                        <div class="profile-table-cell profile-table-name">
                                            <div class="profile-table-cell-inner">
                                                <?php echo get_the_title($id); ?>
                                            </div>
                                        </div>
                                        <!-- .profile-table-cell -->
                                        <div class="profile-table-cell profile-table-designation">
                                            <div class="profile-table-cell-inner">
                                                <?php echo get_post_meta($id,"designation",true); ?>
                                            </div>
                                        </div>
                                        <!-- .profile-table-cell -->
                                        <div class="profile-table-cell profile-table-description">
                                            <div class="profile-table-cell-inner">
                                                <p> <?php echo substr(get_post_field('post_content', $id),0,171); ?> </p>
                                            </div>
                                        </div>
                                        <!-- .profile-table-cell -->
                                        <div class="profile-table-cell">
                                            <ul class="profile-social-icons">
                                                <?php
                                                   foreach($social_media as $b)
                                                     {
														if($b["links"] != '' && $b["types"] != '' ){
															 ?>
															 <li><a href="<?php echo $b['links']; ?>"><i class="<?php echo "fa fa-".$b['types']; ?>" aria-hidden="true"></i></a></li>
															 <?php
														}
                                                      }
                                                ?>
                                            </ul>
                                        </div>
                                        <!-- .profile-table-cell -->
                                    </div>
                                <?php
                            }
                        ?>
                        </div>
                </div>
            </div>
        </div>
    </body>
</html>
