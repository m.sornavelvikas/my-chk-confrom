<?php
    $members_list=explode(',',$members_list);
    if($column_type==2)
    {
        $column_class="codo_teams_slidein_style1_column2";
    }
    else if($column_type==3)
    {
        $column_class="codo_teams_slidein_style1_column3";
    }
    else if($column_type==4)
    {
        $column_class="codo_teams_slidein_style1_column4";
    }
?>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
        <?php echo $custom_css; ?>
        </style>  
    </head> 
    <body>
        <div class="codo_teams codo-slidein style_2  <?php echo 'codoteam_'.$id; ?>">
            <div class="wrapper">
                <div class="elements_container">
                    <?php
                        $i=0; 
                        foreach($members_list as $id)
                        {
                            ?>
                                 <div class="element_container <?php echo $column_class; ?>" data-id="<?php echo $i; ?>" >
                                    <img class="element_img" src="<?php echo get_the_post_thumbnail_url($id); ?>">
                                    <figcaption class="element_personal_info_hover_container">
                                        <div class="element_personal_info_hover">
                                            <span class="element_category_hover codo_hover_color"><?php echo get_post_meta($id,"designation",true); ?></span>
                                            <h3 class="element_name_hove codo_hover_color"><?php echo get_the_title($id); ?></h3>
                                        </div>
                                    </figcaption>
                                </div>
                            <?php
                            $i++;
                        }
                    ?>

                    <div id="mySidenav" class="sidenav">
                        <div class="mySidenav_header">
                            <div class="nav_controls">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </div>
                            <span class="dropdown_close_button">&times;</span>
                        </div>
                        <div class="codo_teams_modals_style_2_sidebar_container">
                            <div class="codo_teams_modals_style_2_sidebar owl-carousel">
                            <?php 
                                    foreach($members_list as $id)
                                    {
                                        ?>
                                        <?php $addl_info=get_post_meta($id,"addl_info",true); ?>
                                        <?php $social_media=get_post_meta($id,"social_media",true); ?>
                                            <div class="content">
                                                <div class="codo_element_slidein_container">
                                                    <div class="dropdown_name_details_container">
                                                            <img class="element_img" src="<?php echo get_the_post_thumbnail_url($id); ?>">
                                                    </div>
                                                    <div class="dropdown_details_container">
                                                        <h3 class="dropdown_category_text"><?php echo get_post_meta($id,"designation",true); ?></h3>
                                                        <h1 class="dropdown_name_text"><?php echo get_the_title($id); ?></h1>
                                                        <p class="dropdown_description"><?php echo get_post_field('post_content', $id); ?></p>
                                                        <div class="element_addl_info"> 
                                                            <?php 
                                                                foreach($addl_info as $a)
                                                                {
																	if($a["label"] != '' && $a["content"] != '' ){
																		?>
																			<p data-addl_info_label="<?php echo $a['label']; ?>"><?php  echo ucwords($a['label'])." : " .$a['content']; ?> </p>
																		<?php
																	 }
                                                                }
                                                            ?>
                                                        </div>
                                                        <span class="dropdown_contact_details_social_icons">
                                                            <?php
                                                                foreach($social_media as $b)
                                                                {
																	
																	if($b["links"] != '' && $b["types"] != '' ){
																		?>
																			<a href="<?php echo $b['links']; ?>" class="<?php echo "dropdown_".$b['types']; ?> social_icons"><i class="<?php echo "fa fa-".$b['types']; ?>" aria-hidden="true"></i></a>
																		<?php
																		}
                                                                }
                                                            ?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
