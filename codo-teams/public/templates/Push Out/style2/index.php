<?php
    $members_list=explode(',',$members_list);
    if($column_type==2)
    {
        $column_class="codo_teams_the_drawer_style2_column2";
    }
    else if($column_type==3)
    {
        $column_class="codo_teams_the_drawer_style2_column3";
    }
    else if($column_type==4)
    {
        $column_class="codo_teams_the_drawer_style2_column4";
    }
?>
<html>
    <head>
        <title>

        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
        <?php echo $custom_css; ?>
        </style>
    </head>
    <body>
        <div class="codo_teams codo_drawer style_2  <?php echo 'codoteam_'.$id; ?>" data-column="<?php echo $column_type; ?>">
            <div class="wrapper">
                <div class="elements_container">
                    <?php
                        $i=0;
                        foreach($members_list as $id)
                        {
                            ?>
                                <?php $addl_info=get_post_meta($id,"addl_info",true); ?>
                                <?php $social_media=get_post_meta($id,"social_media",true); ?>
                                <div class="element_container <?php echo $column_class; ?>" data-index="<?php echo $i+1; ?>">
                                    <img class="element_img" src="<?php echo get_the_post_thumbnail_url($id); ?>">
                                    <figcaption class="element_personal_info_hover_container codo_hover_background">
                                            <div class="element_personal_info_hover codo_hover_color">
                                                <span class="element_category_hover codo_hover_color"> <?php echo get_post_meta($id,"designation",true); ?> </span>
                                                <h3 class="element_name_hover codo_hover_color"> <?php echo get_the_title($id); ?> </h3>
                                            </div>
                                        </figcaption>
                                    <span class="element_category hidden"> <?php echo get_post_meta($id,"designation",true); ?> </span>
                                    <span class="element_name hidden"> <?php echo get_the_title($id); ?> </span>
                                    <span class="element_description hidden"> <?php echo get_post_field('post_content', $id) ?> </span>
                                    <span class="element_addl_info"> 
                                        <?php 
                                            foreach($addl_info as $a)
                                            {
												if($a["label"] != '' && $a["content"] != '' ){
													?>
														<span class="hidden" data-addl_info_label="<?php echo $a['label']; ?>"><?php  echo $a['content']; ?> </span>
													<?php
                                                 }
                                            }
                                        ?>
                                    </span>
                                    <span class="element_social_media_profile">
                                        <?php
                                            foreach($social_media as $b)
                                            {
												if($b["links"] != '' && $b["types"] != '' ){
													?>
														<span class="hidden" data-social_media_profile="<?php echo $b['types']; ?>"><?php  echo $b['links']; ?> </span>
													<?php
                                                }
                                            }
                                        ?>
                                    </span>
                                </div>
                            <?php
                            $i++;
                        }

                    ?>
                </div>
            </div>
        </div>
    </body>
</html>
