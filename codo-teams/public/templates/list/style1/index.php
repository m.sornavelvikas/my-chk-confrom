<?php
    $members_list=explode(',',$members_list);
?>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
        <?php echo $custom_css; ?>
        </style>
    </head> 
    <body>
        <div class="codo_teams codo-list style_1 <?php echo 'codoteam_'.$id; ?>">
            <div class="wrapper">
                <div class="elements_container">
                    <ul class="grid list-style style-1">
                        <?php
                           foreach($members_list as $id)
                           {
                              ?>
                                 <?php $social_media=get_post_meta($id,"social_media",true); ?>
                                 <li class="profile-grid-card">
                                    <figure>
                                       <!-- <span class="profile-grid-holder"> -->
                                       <img alt="img 01" src="<?php echo get_the_post_thumbnail_url($id); ?>">
                                       <figcaption>
                                          <div class="profile-personal-info">
                                             <h3><?php echo get_the_title($id); ?></h3>
                                             <span><?php echo get_post_meta($id,"designation",true); ?></span>                          
                                          </div>
                                          <!-- .profile-personal-info -->
                                          <div class="profile-contact-info">
                                             <p><?php echo get_post_field('post_content', $id); ?></p>
                                             <ul class="profile-social-icons">
                                                <?php
                                                   foreach($social_media as $b)
                                                     {
														if($b["links"] != '' && $b["types"] != '' ){
															?>
															<li><a href="<?php echo $b['links']; ?>"><i class="<?php echo "fa fa-".$b['types']; ?>" aria-hidden="true"></i></a></li>
															<?php
														}
                                                      }
                                                ?>
                                             </ul>
                                          </div>
                                          <!-- .profile-contact-info -->
                                       </figcaption>
                                       <!-- </span> -->
                                       <!-- .profile-grid-holder -->
                                    </figure>
                                 </li>
                              <?php
                           }
                        ?>
                     </ul>
                </div>
            </div>
        </div>
    </body>
</html>
