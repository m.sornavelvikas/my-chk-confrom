<?php
    $members_list=explode(',',$members_list);
    if($column_type==2)
    {
        $column_class="codo_teams_modal_style1_column2";
    }
    else if($column_type==3)
    {
        $column_class="codo_teams_modal_style1_column3";
    }
    else if($column_type==4)
    {
        $column_class="codo_teams_modal_style1_column4";
    }
?>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
        <?php echo $custom_css; ?>
        </style>
    </head>
    <body>
        <div class="codo_teams codo-modals style_1 <?php echo 'codoteam_'.$id; ?>" data-column="<?php echo $column_type; ?>"> 
            <div class="wrapper">
                <div class="elements_container">
                    <?php
                        $i=0; 
                        foreach($members_list as $id)
                        {
                            $i_value=$i+1;
                            ?>
                                <a href="<?php echo "#content".$i_value; ?>" class="fancy" rel="fancy">
                                    <div class="element_container <?php echo $column_class; ?>" data-index="<?php echo $i_value; ?>">
                                        <img class="element_img" src="<?php echo get_the_post_thumbnail_url($id); ?>">
                                        <figcaption class="element_personal_info_hover_container codo_hover_background">
                                                <div class="element_personal_info_hover">
                                                    <span class="element_category_hover codo_hover_color"><?php echo get_post_meta($id,"designation",true); ?></span>
                                                    <h3 class="element_name_hover codo_hover_color"><?php echo get_the_title($id); ?></h3>
                                                </div>
                                         </figcaption>
                                    </div>
                                 </a>
                            <?php
                            $i++;
                        }
                    ?>

                    <div class="codo_teams_modals_style_1" style="display:none">
                        <?php
                            $j=0;
                            foreach($members_list as $id)
                            {
                                $j_value=$j+1;
                                ?>
                                    <?php $addl_info=get_post_meta($id,"addl_info",true); ?>
                                    <?php $social_media=get_post_meta($id,"social_media",true); ?>
                                    <div id="<?php echo "content".$j_value; ?>" class="content">
                                        <div class="element_drawer_container modals_style_1">
                                            <div class="dropdown_name_details_container">
                                                    <img class="element_img" src="<?php echo get_the_post_thumbnail_url($id); ?>">
                                            </div>
                                            <div class="dropdown_details_container">
                                                <h3 class="dropdown_category_text"> <?php echo get_post_meta($id,"designation",true); ?> </h3>
                                                <h1 class="dropdown_name_text"> <?php echo get_the_title($id); ?> </h1>
                                                <p class="dropdown_description codo-model"> <?php echo get_post_field('post_content', $id) ?> </p>
                                                <div class="codo_teams_modal_style1_addl_info"> 
                                                    <?php 
                                                        foreach($addl_info as $a)
                                                        {
															
															if($a["label"] != '' && $a["content"] != '' ){
																?>
																	<p> <?php  echo ucfirst($a["label"])." : ".$a["content"]; ?> </p>
																<?php
															}
                                                        }
                                                    ?>
                                                </div>
                                                <div class="codo_teams_modal_style1_social_media_profile">
                                                    <?php
                                                        foreach($social_media as $b)
                                                        {
															if($b["links"] != '' && $b["types"] != '' ){
																?>
																	<a href="<?php echo $b[links]; ?>" class="<?php echo "dropdown_".$b[types]; ?> social_icons"><i class="<?php echo "fa fa-".$b[types]; ?>" aria-hidden="true"></i></a>
																<?php
															}
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                   </div>
                                <?php
                                $j++;
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
