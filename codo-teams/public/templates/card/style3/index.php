<?php
    $members_list=explode(',',$members_list);
    if($column_type==2)
    {
        $column_class="codo_teams_card_style3_column2";
    }
    else if($column_type==3)
    {
        $column_class="codo_teams_card_style3_column3";
    }
    else if($column_type==4)
    {
        $column_class="codo_teams_card_style3_column4";
    }
?>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
        <?php echo $custom_css; ?>
        </style>
    </head> 
    <body>
        <div class="codo_teams codo-card style_3  <?php echo 'codoteam_'.$id; ?>">
            <div class="wrapper">
                <div class="elements_container">
                    <?php 
                        foreach($members_list as $id)
                        {
                            $social_media=get_post_meta($id,"social_media",true);
                            ?>
                                <div class="element_container <?php echo $column_class; ?>">
                                    <div class="details_container">
                                        <div class="profile_image">
                                            <img class="element_img" src="<?php echo get_the_post_thumbnail_url($id); ?>">
                                        </div>
                                        <figcaption class="element_personal_info_hover_container">
                                            <div class="element_personal_info_hover">
                                                <span class="element_category_hover"> <?php echo get_post_meta($id,"designation",true); ?> </span>
                                                <h3 class="element_name_hover">  <?php echo get_the_title($id); ?>  </h3>
                                            </div>
                                            <div class="codo-contact-info">
                                            <?php
                                                    $codoContent = get_post_field('post_content', $id);
                                                   
                                                ?>
                                                    <p>  <?php echo substr($codoContent,0,111); ?>  </p>
                                                <span class="dropdown_contact_details_social_icons">
                                                     <?php
                                                        foreach($social_media as $b)
                                                        {
															if($b["links"] != '' && $b["types"] != '' ){
																?>
																	<a href="<?php echo $b['links']; ?>" class="<?php echo "dropdown_".$b['types']; ?> social_icons"><i class="<?php echo "fa fa-".$b['types']; ?>" aria-hidden="true"></i></a>
																<?php
															}
                                                        }
                                                    ?>
                                                </span>
                                            </div>
                                        </figcaption>
                                    </div>
                                </div>
                            <?php
                        }
                    ?>
                   
                </div>
            </div>
        </div>
    </body>
</html>
