<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       www.codosome.com
 * @since      1.0.0
 *
 * @package    Codo_Teams
 * @subpackage Codo_Teams/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Codo_Teams
 * @subpackage Codo_Teams/public
 * @author     Codosome <www.codosome.com>
 */
class Codo_Teams_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		add_filter( 'the_content', array( $this ,'adding_template_custom_post_type'), 10, 1 );
		add_filter( 'content_save_pre' ,array( $this , 'update_member_striptags') , 10, 1);
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Codo_Teams_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Codo_Teams_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		
		wp_enqueue_style( 'font-awesome', plugin_dir_url( __FILE__ ) . 'css/font-awesome.min.css', array(),'4.7.0', 'all' );
		wp_enqueue_style( "Fancybox-CSS", plugin_dir_url( __FILE__ ) . 'css/jquery.fancybox.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( "Templates-Presets-CSS", plugin_dir_url( __FILE__ ) . 'css/templates-presets.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'owl-carousel-css', plugin_dir_url( __FILE__ ) . 'css/owl.carousel.min.css', array(),'2.3.4', 'all' );
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Codo_Teams_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Codo_Teams_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		
		wp_enqueue_script( "Fancybox-JS", plugin_dir_url( __FILE__ ) . 'js/jquery.fancybox.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( "Templates-Presets-JS", plugin_dir_url( __FILE__ ) . 'js/templates-presets.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( "FOWl Carosel-JS", plugin_dir_url( __FILE__ ) . 'js/owl.carousel.min.js', array( 'jquery' ),'2.3.4', false );

	}

	function adding_template_custom_post_type($content) {
		if ( 'codo-teams' == get_post_type() ){
			$id = get_the_ID();
			ob_start();
			echo do_shortcode( '[codoteam id="'.$id.'"]' );
			$output = ob_get_contents();
			ob_end_clean();
			$content .= $output;
		}elseif ( 'codo-members' == get_post_type() ){
			$content = $this->stripTags($content);
		}
		
			return $content;
	}
	
	 public function stripTags($content)
    {
        $allowedTags = '<p><strong><em><u><h1><h2><h3><h4><h5><h6>';
		$allowedTags .= '<li><ol><ul><br><ins><del><i><b>';

        $content =  strip_tags($content,$allowedTags);
		$string = htmlentities($content, null, 'utf-8');
		$content = str_replace("&nbsp;", " ", $string);
		$content = html_entity_decode($content);
		return $content;
	}
	public function update_member_striptags( $content ) {
		if ( 'codo-members' == get_post_type() ){
			$content = $this->stripTags($content);
		}
		
		return $content;
	}
}
