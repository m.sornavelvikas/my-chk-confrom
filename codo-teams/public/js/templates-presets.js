(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

    $(document).ready(function(){
        // Modal Prestes - JS 
        $(".fancy").fancybox({
            openEffect : 'changeIn',
            baseTpl:
            '<div class="fancybox-container fancybox-overlay" role="dialog" tabindex="-1">' +
            '<div class="fancybox-bg"></div>' +
            '<div class="fancybox-inner">' +
            '<div class="fancybox-infobar"><span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span></div>' +
            '<div class="fancybox-toolbar">{{buttons}}</div>' +
            '<div class="fancybox-navigation">{{arrows}}</div>' +
            '<div class="fancybox-stage"></div>' +
            '<div class="fancybox-caption"></div>' +
            "</div>" +
            "</div>",
        });

        // Push Out Presets - js - Style 1
        $( ".codo_teams.codo_drawer.style_1 .element_container" ).click( function(){

            if( !$(this).hasClass("expanded") ) {
    
                $(".element_drawer_container").remove();
                
            }
            var category = '';
            var name = '';
            var description = '';
            var addl_info= '';
            var social_media_profile = '';
    
            if( $(this).find(".element_category").text() ) {
                category = '<h3 class="dropdown_category_text">'+$(this).find(".element_category").text()+'</h3>' ;
            }
            if( $(".element_name").text() ) {
                name = '<h1 class="dropdown_name_text">'+$(this).find(".element_name").text()+'</h1>' ;
            }
            if( $(".element_description").text() ) {
                description = '<p class="dropdown_description">'+$(this).find(".element_description").text()+'</p>'  ;
            }
        
            $(this).find(".element_addl_info span").each(function(){
                var $addl_info_label=$(this).data("addl_info_label");
                var $addl_info_content=$(this).text();
                addl_info=addl_info+'<p class="dropdown_add_feilds dropdown_'+$addl_info_label+'">'+$addl_info_label.charAt(0).toUpperCase() + $addl_info_label.slice(1)+' : '+$addl_info_content+'</p>';
            });

            $(this).find(".element_social_media_profile span").each(function(){
                var $social_media_label=$(this).data("social_media_profile");
                var $social_media_content=$(this).text();
                social_media_profile=social_media_profile+'<a href="'+$social_media_content+'" class="dropdown_'+$social_media_label+' social_icons"><i class="fa fa-'+$social_media_label+'" aria-hidden="true"></i></a>';
            });
    
            var close_button = '<span class="dropdown_close_button">&times;</span>' ;
        
            $(this).after("<div class='element_drawer_container' style='display:none'><div class='dropdown_name_details_container'>"+category+name+description+"</div><div class='dropdown_contact_details_container'>"+close_button+addl_info+'<div class="dropdown_contact_details_social_icons">'+social_media_profile+"</div></div></div>");
           
            if( $(this).hasClass("expanded") ) { 
                    
                $(".codo_teams.codo_drawer.style_1 .elements_container").find(".element_container").removeClass("expanded"); 
    
                $('.codo_teams.codo_drawer.style_1 .element_drawer_container').slideUp();
    
            } else {
    
                if( $(".elements_container").find(".element_container").hasClass("expanded") ){
        
                    $(".codo_teams.codo_drawer.style_1 .element_personal_info_hover_container").addClass("changed");               
    
                    var expanded_value = '';
                    var clicked_value = '';
                    var if_or_else = 'false';
    
                    if( $(window).width() > 1050 ) {
                        expanded_value = Math.ceil( $(".codo_teams.codo_drawer.style_1 .element_container.expanded").data('index')/4 );

                        clicked_value =  Math.ceil( $(this).data('index')/4 );
    
                    } else if(  $(window).width() > 850 && $(window).width() < 1050 ) {
    
                        expanded_value = Math.ceil( $(".codo_teams.codo_drawer.style_1 .element_container.expanded").data('index')/3 );
    
                        clicked_value =  Math.ceil( $(this).data('index')/3 );
    
                    } else if(  $(window).width() > 600 && $(window).width() < 850 ) {
    
                        expanded_value = Math.ceil( $(".codo_teams.codo_drawer.style_1 .element_container.expanded").data('index')/2 );
    
                        clicked_value =  Math.ceil( $(this).data('index')/2 );
    
                    } else {
    
                        if_or_else = 'true';
    
                    }
    
                    if(if_or_else != 'true') {
    
                        if(  expanded_value == clicked_value ) { 
    
                            $(".codo_teams.codo_drawer.style_1 .elements_container").find(".element_container").removeClass("expanded");
    
                            $(this).addClass("expanded");
            
                            $('.codo_teams.codo_drawer.style_1 .element_drawer_container').css("display","block");
    
                        } else {
                            
                            $(".codo_teams.codo_drawer.style_1 .elements_container").find(".element_container").removeClass("expanded");
    
                            $(this).addClass("expanded"); 
    
                            $('.codo_teams.codo_drawer.style_1 .element_drawer_container').slideDown();
    
                        }
    
                    } else {
    
                        $(".codo_teams.codo_drawer.style_1 .elements_container").find(".element_container").removeClass("expanded");
    
                        $(this).addClass("expanded"); 
    
                        $('.codo_teams.codo_drawer.style_1 .element_drawer_container').slideDown();
    
                    }
    
                } else {
                    
                    $(this).addClass("expanded"); 
    
                    $('.codo_teams.codo_drawer.style_1 .element_drawer_container').slideDown();
                
                }
    
            }
                
            $('span.dropdown_close_button').click( function(){
                    
                $(".codo_teams.codo_drawer.style_1 .elements_container").find(".element_container").removeClass("expanded"); 
    
                $('.codo_teams.codo_drawer.style_1 .element_drawer_container').slideUp();
    
            } );
    
        } );
    
        //  Push Out Presets JS - Style 2 
        $( ".codo_teams.codo_drawer.style_2 .element_container" ).click( function(){

            

            if( !$(this).hasClass("expanded") ) {
    
                $(".element_drawer_container").remove();
                
            }
    
            var category = '';
            var name = '';
            var description = '';
            var addl_info= '';
            var social_media_profile = '';
    
            if( $(this).find(".element_category").text() ) {
                category = '<h3 class="dropdown_category_text">'+$(this).find(".element_category").text()+'</h3>' ;
            }
            if( $(".element_name").text() ) {
                name = '<h1 class="dropdown_name_text">'+$(this).find(".element_name").text()+'</h1>' ;
            }
            if( $(".element_description").text() ) {
                description = '<p class="dropdown_description">'+$(this).find(".element_description").text()+'</p>'  ;
            }
            
            $(this).find(".element_addl_info span").each(function(){
                var $addl_info_label=$(this).data("addl_info_label");
                var $addl_info_content=$(this).text();
                addl_info=addl_info+'<p class="dropdown_'+$addl_info_label+'">'+$addl_info_label.charAt(0).toUpperCase() + $addl_info_label.slice(1)+' : '+$addl_info_content+'</p>';
            });

            $(this).find(".element_social_media_profile span").each(function(){
                var $social_media_label=$(this).data("social_media_profile");
                var $social_media_content=$(this).text();
                social_media_profile=social_media_profile+'<a href="'+$social_media_content+'" class="dropdown_'+$social_media_label+' social_icons"><i class="fa fa-'+$social_media_label+'" aria-hidden="true"></i></a>';
            });
    
            var close_button = '<span class="dropdown_close_button">&times;</span>' ;
        
            $(this).after("<div class='element_drawer_container' style='display:none'><div class='dropdown_name_details_container'>"+category+name+description+"</div><div class='dropdown_contact_details_container'>"+close_button+addl_info+'<div class="dropdown_contact_details_social_icons">'+social_media_profile+"</div></div></div>");
           
            if( $(this).hasClass("expanded") ) { 
                    
                $(".codo_teams.codo_drawer.style_2 .elements_container").find(".element_container").removeClass("expanded"); 
    
                $('.codo_teams.codo_drawer.style_2 .element_drawer_container').slideUp();
    
            } else {
    
                if( $(".elements_container").find(".element_container").hasClass("expanded") ){
        
                    $(".codo_teams.codo_drawer.style_2 .element_personal_info_hover_container").addClass("changed");               
    
                    var expanded_value = '';
                    var clicked_value = '';
                    var if_or_else = 'false';
    
                    if( $(window).width() > 1050 ) {
    
                        expanded_value = Math.ceil( $(".codo_teams.codo_drawer.style_2 .element_container.expanded").data('index')/4 );
    
                        clicked_value =  Math.ceil( $(this).data('index')/4 );
    
                    } else if(  $(window).width() > 850 && $(window).width() < 1050 ) {
    
                        expanded_value = Math.ceil( $(".codo_teams.codo_drawer.style_2 .element_container.expanded").data('index')/3 );
    
                        clicked_value =  Math.ceil( $(this).data('index')/3 );
    
                    } else if(  $(window).width() > 600 && $(window).width() < 850 ) {
    
                        expanded_value = Math.ceil( $(".codo_teams.codo_drawer.style_2 .element_container.expanded").data('index')/2 );
    
                        clicked_value =  Math.ceil( $(this).data('index')/2 );
    
                    } else {
    
                        if_or_else = 'true';
    
                    }
    
                    if(if_or_else != 'true') {
    
                        if(  expanded_value == clicked_value ) { 
    
                            $(".codo_teams.codo_drawer.style_2 .elements_container").find(".element_container").removeClass("expanded");
    
                            $(this).addClass("expanded");
            
                            $('.codo_teams.codo_drawer.style_2 .element_drawer_container').css("display","block");
    
                        } else {
                            
                            $(".codo_teams.codo_drawer.style_2 .elements_container").find(".element_container").removeClass("expanded");
    
                            $(this).addClass("expanded"); 
    
                            $('.codo_teams.codo_drawer.style_2 .element_drawer_container').slideDown();
    
                        }
    
                    } else {
    
                        $(".codo_teams.codo_drawer.style_2 .elements_container").find(".element_container").removeClass("expanded");
    
                        $(this).addClass("expanded"); 
    
                        $('.codo_teams.codo_drawer.style_2 .element_drawer_container').slideDown();
    
                    }
    
                } else {
                    
                    $(this).addClass("expanded"); 
    
                    $(".codo_teams.codo_drawer.style_2 .element_personal_info_hover_container").addClass("changed");
    
                    $('.codo_teams.codo_drawer.style_2 .element_drawer_container').slideDown();
                
                }
    
            }
                
            $('span.dropdown_close_button').click( function(){
                    
                $(".codo_teams.codo_drawer.style_2 .elements_container").find(".element_container").removeClass("expanded"); 
    
                $('.codo_teams.codo_drawer.style_2 .element_drawer_container').slideUp();
    
            } );
    
        } );

        // The Slidein Presets Js - Style 1
        $('.codo_teams.codo-slidein.style_1 .owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            items: 1
        });
        
        $('.codo_teams.codo-slidein.style_1 .owl-carousel').owlCarousel();
        
        $( ".codo_teams.codo-slidein.style_1 .element_container" ).click( function(e){
        
            e.stopPropagation();
			
            $('.codo_teams.codo-slidein.style_1 .owl-carousel').trigger('to.owl.carousel', $(this).data("id") );
        
            setTimeout(
        
                function() {
        
                    $("body").delay(1000).addClass("codo_sidebar_opened");
        
                    $( "body.codo_sidebar_opened" ).append( "<div class='slidein_style_1_background_blur'></div> ");
                    
                    $(".codo_sidebar_opened .codo_teams.codo-slidein.style_1 .sidenav").delay(1000).css('right','0px');
        
                },
        
            400); 
            
        });
        
        $( "body").on("click",".slidein_style_1_background_blur", function(){
			
			$(".codo_sidebar_opened .codo_teams.codo-slidein.style_1 .sidenav").delay(1000).css('right','-500px');
        
            $( "body.codo_sidebar_opened .slidein_style_1_background_blur" ).remove();
        
            $("body").removeClass("codo_sidebar_opened");
        
        } );
        
        
        $(".codo_teams.codo-slidein.style_1 .dropdown_close_button").click( function(){ 
			
			$(".codo_sidebar_opened .codo_teams.codo-slidein.style_1 .sidenav").delay(1000).css('right','-500px');
        
            $( "body.codo_sidebar_opened .slidein_style_1_background_blur" ).remove();
        
            $("body").removeClass("codo_sidebar_opened");
        
        } );
        
        $( ".codo_teams.codo-slidein.style_1 i.fa.fa-angle-left" ).click( function(){
            $('.codo_teams.codo-slidein.style_1 .owl-carousel').trigger('prev.owl.carousel');
        
        });
        
        $( ".codo_teams.codo-slidein.style_1 i.fa.fa-angle-right" ).click( function(){
            $('.codo_teams.codo-slidein.style_1 .owl-carousel').trigger('next.owl.carousel');
        
        });
    
        // The Slidein Presets Js -Style 2
        $('.codo_teams.codo-slidein.style_2 .owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            items: 1
        });
        
        
        $('.codo_teams.codo-slidein.style_2 .owl-carousel').owlCarousel();
        
        $( ".codo_teams.codo-slidein.style_2 .element_container" ).click( function(e){
        
            e.stopPropagation();
        
            $('.codo_teams.codo-slidein.style_2 .owl-carousel').trigger('to.owl.carousel', $(this).data("id") );
        
            setTimeout(
        
                function() {
        
                    $("body").delay(1000).addClass("codo_sidebar_opened");
        
                    $( "body.codo_sidebar_opened" ).append( "<div class='slidein_style_2_background_blur'></div> ");
                    
                     $(".codo_sidebar_opened .codo_teams.codo-slidein.style_2 .sidenav").delay(1000).css('left','0px');
        
                },
        
            400); 
            
        });
        
        $( "body").on("click",".slidein_style_2_background_blur", function(){
        
        
            $( "body.codo_sidebar_opened .slidein_style_2_background_blur" ).remove();
            
			$(".codo_sidebar_opened .codo_teams.codo-slidein.style_2 .sidenav").delay(1000).css('left','-500px');
        
            $("body").removeClass("codo_sidebar_opened");
        
        } );
        
        
        $(".codo_teams.codo-slidein.style_2 .dropdown_close_button").click( function(){ 
        
			$(".codo_sidebar_opened .codo_teams.codo-slidein.style_2 .sidenav").delay(1000).css('left','-500px');
			
            $( "body.codo_sidebar_opened .slidein_style_2_background_blur" ).remove();
        
            $("body").removeClass("codo_sidebar_opened");
        
        } );
        
        $( ".codo_teams.codo-slidein.style_2 i.fa.fa-angle-left" ).click( function(){
        
            $('.codo_teams.codo-slidein.style_2 .owl-carousel').trigger('prev.owl.carousel');
        
        });
        
        $( ".codo_teams.codo-slidein.style_2 i.fa.fa-angle-right" ).click( function(){
        
            $('.codo_teams.codo-slidein.style_2 .owl-carousel').trigger('next.owl.carousel'); 
        
        });
    });


})( jQuery );
