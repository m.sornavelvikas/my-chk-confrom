=== Codo Teams ===
Contributors: Codosome
Donate link: 
Tags: Teams, List Teams, Teams management, company team
Requires at least: 4.0
Tested up to: 4.8
Stable tag: trunk
Requires PHP: 5.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Insert your team members as a grid or any other layout you prefer from our list of layouts. 

== Description ==

Insert your team members as a grid or any other layout you prefer from our list of layouts.

It supports various layouts like Grid, Cards, Modal Popup, List, Circular, SlideIn and Table layouts.

Members can be inserted into various templates using a dropdown. And the layouts can be inserted using shortcodes. So its easy to manage.

Have an option to import the demo content.

Easily customisable with font size, color and background color. It also supports custom css. 

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the Codo Teams -> Settings screen to import the demo
4. Use shortcode from the Teams section to insert in post or page

== Frequently Asked Questions ==

= How to import demo ? =

In the Codo Teams settings you will find the import button which will import the preconfigured members and teams

= How to insert a team in page or post ? =

The team can be inserted using the shortcode in any post or page. The short code can be found on the teams section.


== Screenshots ==

1. 
2. 

== Changelog ==


== Upgrade Notice ==

