<?php
class codoSettingPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ),999 );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
	
    {	
		
		add_submenu_page( 'edit.php?post_type=codo-members', 'Settings', 'Settings','manage_options', 'codo_teams_settings', array( $this, 'create_admin_page' ));

 
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'codo_import_templates' );
        ?>
        <div class="wrap">
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'codo_teams_settings_group' );
                do_settings_sections( 'codo_teams_settings' );
            ?>
			 <div class="codo-teams-import-button">
             <button class="button-primary"><span>Import Demo Content</span></button>
             </div>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'codo_teams_settings_group', // Option group
            'codo_import_templates' // Option name
        );

        add_settings_section(
            'setting_section_id', // ID
            'Codo Teams Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'codo_teams_settings' // Page
        );

       
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'To import demo click the button';
    }

   
}

if( is_admin() )
    $my_settings_page = new codoSettingPage();
