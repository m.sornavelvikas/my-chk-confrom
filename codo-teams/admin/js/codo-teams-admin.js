(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	$(document).ready(function(){

		$("#additional_information tbody").sortable();
		
		$("#social_media_profile tbody").sortable();
			 
		// Additional Information add the New row
		 $("#add_additional_info").click(function(e){                                 
			e.preventDefault();
			var $trLast = $("#additional_information tbody tr:last-child");
			var $newrow = $trLast.clone();
			var $index = $newrow.data('index');
			var $index_va=parseInt($index)+1;
			$newrow.attr('data-index',$index_va);
			$newrow.find('td:first-child span:first-child').html($index_va);
			$newrow.find('input').each(function(){
				$(this).prop('required',false);
				var $val=$(this).attr('name');
				var $attr=$(this).attr('data-classname');
				var $val='addl_info['+$index+']['+$attr+']';
				$(this).attr('name',$val);
				if(($(this).attr('type') == 'text'))
				{
					$(this).val(null);
				}
			});
			$trLast.after($newrow);
		});
		
		
		// Additional Information table Delete the Row
		$("#additional_information").on("click", ".remove_btn", function (event) {                   
			$(this).closest("tr").remove();       
		});
		
		
		// Image Load in Dropdown box - Social Media
		var formatResult2 = function (state) {                                                      
			if (!$(state.element).data("url")) {
				return state.text;
			}
			var imgae_url=$(state.element).data("url");
			return $("<img src='"+imgae_url+"' alt='image_da' class='option_image_social_media'><div class='option_text_social_media'>" + state.text + "</div>");
		};
	
		var formatResult3 = function (state) {                                                      
			if (!$(state.element).data("url")) {
				return state.text;
			}
			var imgae_url=$(state.element).data("url");
			return $("<img src='"+imgae_url+"' alt='image_da' class='selected_image_social_media'><div class='option_text_social_media'>" + state.text + "</div>");
		};
		

		// Social Media Profile Add the New Row
		var $trLast = $("#social_media_profile tbody tr:last-child");
		var $newrow_clone = $trLast.clone();
		$("#add_social_media").click(function(e){                                                    
				e.preventDefault();
				var $newrow = $newrow_clone.clone();
				var $tr_last_index = $("#social_media_profile tbody tr:last-child").data('index');
				var $index_va=parseInt($tr_last_index)+1;
				$newrow.attr('data-index',$index_va);
				$newrow.find('td:first-child span:first-child').html($index_va);
				$newrow.find('input').each(function(){
					$(this).prop('required',false);
					var $val=$(this).attr('name');
					var $attr=$(this).attr('data-classname');
					var $val='social_media['+$tr_last_index+']['+$attr+']';
					$(this).attr('name',$val);
					if(($(this).attr('type') == 'text'))
					{
						$(this).val(null);
					}
				});
				$newrow.find('.select2-container').remove();
				$newrow.find('select').each(function(){
					var $val=$(this).attr('name');
					var $attr=$(this).attr('data-classname');
					var $val='social_media['+$tr_last_index+']['+$attr+']';
					$(this).attr('name',$val);
					$(this).select2({
						templateResult: formatResult2,
						templateSelection: formatResult3,
						width:'100%'
					});
				});
				$("#social_media_profile tbody tr:last-child").after($newrow);
		});
	

		// Social Media Profile Delete the Row
		$("#social_media_profile").on("click", ".remove_social_media", function (event) {                     
			$(this).closest("tr").remove();       
		});
	
			
		//  Image Load in Dropdown box - Social Media
		$("#social_media_profile").find('select').each(function(){                                            
			$(this).select2({ 
				templateResult: formatResult2,
				templateSelection: formatResult3,
				width:'100%',
			});
		});
	

		// Image Load in Dropdown box - Member
		var formatResult = function (state) {                                                                  
			if (!$(state.element).data("url")) {
				return state.text;
			}
			var imgae_url=$(state.element).data("url");
			return $("<div class='option_image'><img class='flag' src='"+imgae_url+"' alt='image_da' style='border-radius: 50%;height:35px;width:35px;'></div><div class='option_text'>" + state.text + "</div>");
		};
		

		$('#select-member').select2({
			templateResult: formatResult,
			width:'100%',
		});

	
		// On Load Split the Member to various option group
		if ($("#thedata").length>0) {
		var member_id_textbox_value=$("#thedata").val().split(",");
		$('#select-member optgroup[class="select_a_member"] option').each(function(){
			var select_value=$(this).attr('value');
			var select_image=$(this).attr('data-url');
			var select_text=$(this).text();
			$.each(member_id_textbox_value, function( index, value ) {
				if(select_value==value)
				{
					$('#select-member optgroup[class="select_a_member"] option[value="'+select_value+'"]').remove();
					$('#select-member optgroup[class="already_added"]').append('<option value='+select_value+' data-url='+select_image+'>'+select_text+'</option>');
				}
			});
		});
		}


		// Add Member in the Team
		$('#select-member').change(function(e){
			e.stopImmediatePropagation();
			var option_value=$(this).val();
			var option_image_path=$(this).find(':selected').attr('data-url');
			var option_title=$(this).find(':selected').text();
			var optgroup_class=$(this).find(':selected').parent().attr('class');
			var Lilist=$('#member-list li:last-child');
			var $newrow = Lilist.clone();
			var $index = $newrow.data('index');
			if(optgroup_class=="select_a_member")
			{
				$('#select-member optgroup[class="select_a_member"] option[value="'+option_value+'"]').remove();
				$('#select-member optgroup[class="already_added"]').append('<option value='+option_value+' data-url='+option_image_path+'>'+option_title+'</option>');
			}
			else if(optgroup_class=="already_added")
			{
				$('#select-member optgroup[class="already_added"] option[value="'+option_value+'"]').remove();
				$('#select-member optgroup[class="select_a_member"]').append('<option value='+option_value+' data-url='+option_image_path+'>'+option_title+'</option>');
			}
			$("#member-list li").remove();
			$('#select-member optgroup[class="already_added"] option').each(function(){
				var option_value=$(this).attr('value');
				var option_image_path=$(this).attr('data-url');
				var option_title=$(this).text();
				if($index==null)
				{
					$("#member-list").append('<li id="member_'+option_value+'" data-index="'+option_value+'"><div class="member_list_show"><img class="member_photo" src='+option_image_path+'></img><span class="member_title">'+option_title+'</span><a class="remove_member"></a></div></li>');	
				}
				else
				{
					var $index_va=parseInt($index)+1;
					$newrow.attr('index',$index_va);
					$("#member-list").append('<li id="member_'+option_value+'" data-index="'+option_value+'"><div class="member_list_show"><img class="member_photo" src='+option_image_path+'></img><span class="member_title">'+option_title+'</span><a class="remove_member"></a></div></li>');	 
				}
				$('#thedata').val( $( "#member-list" ).sortable( "toArray", { attribute: "data-index" } ));
			});
			$('#select-member').select2("destroy").select2({
				templateResult: formatResult,
				width:'100%',
			});	
		});
		

		//  Member-list Sortable
		$("#member-list").sortable({                                                                                 
				update:  function (event, ui) {
					$('#thedata').val( $( "#member-list" ).sortable( "toArray", { attribute: "data-index" } ));
				}
		});
		
		
		// Remove the Member in Team
		$("#member-list").on("click", ".remove_member", function (event) {                                             
			var remove_option_id=$(this).closest("li").attr("data-index");
			var remove_option_image=$(this).closest("li").find("img").attr("src");
			var remove_option_text=$(this).closest("li").text();
			$('#select-member optgroup[class="already_added"] option[value="'+remove_option_id+'"]').remove();
			$('#select-member optgroup[class="select_a_member"]').append('<option value='+remove_option_id+' data-url='+remove_option_image+'>'+remove_option_text+'</option>');
			$(this).closest("li").remove();	
			$('#thedata').val( $( "#member-list" ).sortable( "toArray", { attribute: "data-index" } ));
			$('#select-member').select2("destroy").select2({
				templateResult: formatResult,
				width:'100%',
			});
		});
		
		
		
		// Add CSS-Class Select the presets & append the Option value Choose For presests type
		$("#choose_presets_ul span .codo_presets_image").click(function() {
			$("#styles_type").find('option').remove();
			var total_style_folder=$(this).data('stylefolder');
            $.each(total_style_folder, function(index, value) {
				$("#styles_type").append("<option value="+value+">"+value+"</option>");
            });
			
			if($(this).text().trim()=="list" || $(this).text().trim()=="table")
			{
				$("#cloumns_type").prop("disabled", true);
			}
			else
			{
				$("#cloumns_type").prop("disabled", false);
			}
			
			if($('#choose_presets_ul span .codo_presets_image').hasClass('selected')==true)
			{
				$('#choose_presets_ul span .codo_presets_image').removeClass('selected');
				$('#choose_presets_ul span .codo_presets_image').addClass('border_class');
			}
			$(this).addClass('selected');
			$(this).removeClass('border_class');
			$("#choosed_presets").val($.trim($('#choose_presets_ul span .codo_presets_image.selected').text()));
		});

		
		// Copy to Clipboard Using Shortcode
		$(".shortcode.column-shortcode #copy_to_clipboard_shortcode").click(function(e){
			e.preventDefault();
			var container = $(this).parents('.shortcode.column-shortcode');
			console.log(container);
			container.find(".copied_show").show();
			setTimeout(function() {  container.find(".copied_show").hide(); }, 3000);
			var $temp = $("<input>");
			$("body").append($temp);
			$temp.val(container.find(".shortcode_show").text()).select();
			document.execCommand("copy");
			$temp.remove();
		});

		$('body').on('change', '.codo_member_info', function() {
			if($(this).val() !== ""){
				$(this).parents(".codo_info_container").find(".codo_member_info_content").prop('required',true);
			}else{
				$(this).parents(".codo_info_container").find(".codo_member_info_content").prop('required',false);
			}
		});
		
		$('body').on('change', '.social_media_table_selectbox', function() {
			console.log($(this).val());
			if($(this).val() !== ""){
				$(this).parents(".codo_social_selectbox_container").find(".social_media_table_textbox").prop('required',true);
			}else{
				$(this).parents(".codo_social_selectbox_container").find(".social_media_table_textbox").prop('required',false);
			}
		});

		$('#font_color_codo_content').colorPicker();
		$('#font_backgound_hover_codo_content').colorPicker();
			$(".codo-teams-import-button button").click(function(e){
		e.preventDefault();
		jQuery.ajax({
			url : ajax_object.ajax_url,
			type : 'post',
			beforeSend: function() {
				$(".codo-teams-import-button span").addClass("admin_import_loader");
				$(".codo-teams-import-button button").attr("disabled", "disabled");
			},
			data : {
				action : 'codo_import_click'
			},
			success : function( response ) {
				 $(".codo-teams-import-button span").removeClass("admin_import_loader");
				 $('#wpbody-content').prepend('<div class="notice notice-success is-dismissible"><p>Content imported Successfully.</p></div>');
				 $(".codo-teams-import-button button").removeAttr("disabled");
				 
			}
		});
		
	});
		  

	});


})( jQuery );
