<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       www.codosome.com
 * @since      1.0.0
 *
 * @package    Codo_Teams
 * @subpackage Codo_Teams/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Codo_Teams
 * @subpackage Codo_Teams/admin
 * @author     Codosome <www.codosome.com>
 */
class Codo_Teams_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		
		add_action( 'init', array( $this ,'codo_team_post_types') );                         
		add_action( 'init', array( $this ,'add_feature_image') );                         
		add_action( 'admin_menu', array( $this ,'add_new_post_submenu_page' ));
		add_filter( 'enter_title_here', array( $this ,'change_post_default_title' ));
		add_action('admin_init',array($this,'create_meta_box_for_members'));
		add_action( 'save_post',array( $this, 'save_custom_fields_for_memebers' ) );
		add_action( 'save_post',array( $this, 'save_custom_fields_for_teams' ) );
		add_filter('manage_codo-members_posts_columns',array( $this,'custom_columns'));
		add_action( 'manage_codo-members_posts_custom_column' , array( $this,'custom_columns_data'), 10, 2 ); 
		add_filter( 'manage_codo-teams_posts_columns', array( $this,'set_shortcode_columns_in_terms' ));
		add_filter( 'manage_codo-teams_posts_custom_column', array( $this,'set_shortcode_data' ), 19, 2);
		add_action('admin_init',array($this,'create_meta_box_for_teams'));
		add_shortcode( "codoteam", array($this,"codoteam"));
		add_action( 'wp_ajax_codo_import_click', array($this,"codo_import_function"));

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles($page) {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Codo_Teams_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Codo_Teams_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/codo-teams-admin.min.css', array(), $this->version, 'all' );

		wp_enqueue_style( 'jquery-ui-css-codotems', plugin_dir_url( __FILE__ ) . 'css/jquery-ui.min.css', array(), $this->version, 'all' );

		wp_enqueue_style( 'select2.min-css-codotems', plugin_dir_url( __FILE__ ) . 'css/select2.min.css', array(),'4.0.6.0', 'all' );
		

		if($page == 'codo-members_page_codo_teams_settings'){

			wp_enqueue_style( 'font-awesome', plugin_dir_url( __FILE__ ) . 'css/font-awesome.min.css', array(),'4.7.0', 'all' );
		}
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Codo_Teams_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Codo_Teams_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		
		wp_enqueue_script( 'jquery-ui-js-codoteams', plugin_dir_url( __FILE__ ) . 'js/jquery-ui.min.js', array( 'jquery' ), '1.12.1', false );
		wp_enqueue_script( 'select2-min-js-codoteams', plugin_dir_url( __FILE__ ) . 'js/select2.min.js', array( 'jquery' ), '4.0.6.0', false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/codo-teams-admin.min.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'ajax_object',array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_enqueue_script( 'jqColorPicker', plugin_dir_url( __FILE__ ) . 'js/jqColorPicker.min.js', array( 'jquery' ), $this->version, false );
	
	}
	

	//Create Custom Post type For members & teams
	function codo_team_post_types()
	{
		register_post_type( 'codo-members', array(
			'public' => true,
			'labels' => array(
				'name'      => 'CODO - Teams',
				'all_items' => 'Members',
				'add_new' => 'Add New Member',
				'add_new_item' => 'Add New Member',
				'singular_name' =>	'Member',
				'edit_item' =>	'Edit Member',
				'new_item'  =>	'New Member',
				'view_item' =>	'View Member',
				'not_found_in_trash'  => 'No Members found in trash.',
			),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'supports' => array( 'title', 'editor','thumbnail' ),
			'menu_icon' => 'dashicons-groups'
		));	
		
		register_post_type( 'codo-teams', array(
				'public' => true,
				'labels'	=>	array(
								'all_items' => 	'Teams',
								'menu_name'	=>	'Teams',
								'add_new' => 'Add New Teams',
								'add_new_item' => 'Add New Teams',
								'singular_name' =>	'Teams',
								'edit_item' =>	'Edit teams',
								'new_item'  =>	'New Teams',
								'view_item' =>	'View Team',
								'items_archive' =>	'Team Archive',
								'search_items'  =>	'Search team',
								'not_found'	    =>	'No teams found.',
								'not_found_in_trash'  => 'No teams found in trash.'
							),
			'supports'      =>	array( 'title', 'revisions' ),				
			'show_in_menu'  =>	'edit.php?post_type=codo-members',         
			'public'		    =>	true
		));
		
		flush_rewrite_rules();
	}
	
	// Add sub menu page For CODO-Teams
	function add_new_post_submenu_page(){
		add_submenu_page('edit.php?post_type=codo-members', 'Add New Team', 'Add New Team', 'manage_options', 'post-new.php?post_type=codo-teams');
	}
	 
	// Change post Default title
	function change_post_default_title( $title ) {
		$screen = get_current_screen();
		if ( 'codo-members' == $screen->post_type ) {
			$title = 'Enter the Team Member Name Here'; //change this to whatever you’d like
		}
		if ( 'codo-teams' == $screen->post_type ) {
			$title = 'Enter the Team Name Here'; //change this to whatever you’d like
		}	
		return $title;
	}


	// Add Feature Image in Custom post type - members
	function add_feature_image() {
		add_theme_support( 'post-thumbnails' );
		add_post_type_support( 'codo-members', 'thumbnail' );
		post_type_supports( 'codo-members', 'thumbnail' );
		set_post_thumbnail_size( 765, 765, true );
		
	}

	// Create Meta Box For Members 
	function create_meta_box_for_members()
	{
		add_meta_box("member_details","Member Details","create_meta_box_field_for_member","codo-members","normal","high");
		function create_meta_box_field_for_member($object)
		{
			?>
			<div class="=member_details_div">
				<div class="member_desination_div">
					<p class="title_name"> Designation : </p> 
					<input type="text" class="designation" name="designation" value="<?php echo get_post_meta($object->ID, "designation", true); ?>"><br><br>
				</div>		
				<div class="members_addl_information_div">
					<p class="title_name"> Additional Information (For Email,Telephone etc) </p> 
					<table id="additional_information">
						<thead>
							<tr>
								<td> </td>
								<td> Label </td>
								<td> Content </td>
								<td> </td>
							</tr>
						</thead>
						<tbody>
							<?php
								$addl_info=get_post_meta($object->ID, "addl_info", true);
								if(empty($addl_info))
								{
									$addl_info = array(
														array(
															'label' => '',
															'content' => ''
														)
													);
									
								}
								$i=0;
								foreach($addl_info as $a)
								{
									?>
									<tr class="codo_info_container" data-index="<?php echo $i+1; ?>">
										<td> <svg width="18" height="18" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" role="img" transform='rotate(90)' aria-hidden="true" focusable="false"><path d="M13,8c0.6,0,1-0.4,1-1s-0.4-1-1-1s-1,0.4-1,1S12.4,8,13,8z M5,6C4.4,6,4,6.4,4,7s0.4,1,1,1s1-0.4,1-1S5.6,6,5,6z M5,10 c-0.6,0-1,0.4-1,1s0.4,1,1,1s1-0.4,1-1S5.6,10,5,10z M13,10c-0.6,0-1,0.4-1,1s0.4,1,1,1s1-0.4,1-1S13.6,10,13,10z M9,6 C8.4,6,8,6.4,8,7s0.4,1,1,1s1-0.4,1-1S9.6,6,9,6z M9,10c-0.6,0-1,0.4-1,1s0.4,1,1,1s1-0.4,1-1S9.6,10,9,10z"></path></svg></td>
										<td> <input type="text" value="<?php echo $a['label']; ?>" class="addl_info_textbox codo_member_info" name="<?php echo 'addl_info['.$i.'][label]'; ?>" data-classname="label" placeholder="Ex:email"></td>
										<td> <input type="text" value="<?php echo $a['content']; ?>" class="addl_info_textbox codo_member_info_content" name="<?php echo 'addl_info['.$i.'][content]'; ?>" data-classname="content" placeholder="Ex:example@gmail.com"></td>
										<td> <button class="remove_btn"> Remove </button></td>
									</tr>
									<?php
									$i++;
								}
						?>
						</tbody>
					</table>
					<button id="add_additional_info"> Add Row </button>
				</div>
				<div class="members_social_media_div">					
					<p class="title_name">Social Links (Twitter,Linkedin,etc) </p>
					<table id="social_media_profile">
						<thead>
							<tr>
								<td> </td>
								<td> Icon </td>
								<td> Link </td>
								<td> </td>
							</tr>
						</thead>
						<tbody>
							<?php
								$social_media=get_post_meta($object->ID, "social_media", true);
								if(empty($social_media))
								{
									$social_media = array(
														array(
															'types' => '',
															'links' => ''
														)
													);
									
								}
								$i=0;
								foreach($social_media as $b)
								{
									?>
									<tr class="codo_social_selectbox_container" data-index="<?php echo $i+1; ?>">
										<td> <svg width="18" height="18" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" role="img" transform='rotate(90)' aria-hidden="true" focusable="false"><path d="M13,8c0.6,0,1-0.4,1-1s-0.4-1-1-1s-1,0.4-1,1S12.4,8,13,8z M5,6C4.4,6,4,6.4,4,7s0.4,1,1,1s1-0.4,1-1S5.6,6,5,6z M5,10 c-0.6,0-1,0.4-1,1s0.4,1,1,1s1-0.4,1-1S5.6,10,5,10z M13,10c-0.6,0-1,0.4-1,1s0.4,1,1,1s1-0.4,1-1S13.6,10,13,10z M9,6 C8.4,6,8,6.4,8,7s0.4,1,1,1s1-0.4,1-1S9.6,6,9,6z M9,10c-0.6,0-1,0.4-1,1s0.4,1,1,1s1-0.4,1-1S9.6,10,9,10z"></path></svg></td>
										<td> 
											<select name="<?php echo 'social_media['.$i.'][types]'; ?>" class="social_media_table_selectbox" data-classname="types">
												<option value=""> Select Social Media </option>
												<option value="facebook" data-url="<?php echo plugin_dir_url( __FILE__ ). "images/facebook.png"; ?>" <?php if($b['types']=="facebook") echo "selected"; ?> > Facebook </option>
												<option value="twitter" data-url="<?php echo plugin_dir_url( __FILE__ ). "images/twitter.png"; ?>" <?php if($b['types']=="twitter") echo "selected"; ?> > Twitter </option>
												<option value="instagram" data-url="<?php echo plugin_dir_url( __FILE__ ). "images/instagram.png"; ?>" <?php if($b['types']=="instagram") echo "selected"; ?> > Instagram </option>
												<option value="envelope" data-url="<?php echo plugin_dir_url( __FILE__ ). "images/mail.png"; ?>" <?php if($b['types']=="envelope") echo "selected"; ?> > Mail </option>
												<option value="google-plus" data-url="<?php echo plugin_dir_url( __FILE__ ). "images/google_plus.png"; ?>" <?php if($b['types']=="google-plus") echo "selected"; ?> > Google Plus </option>
												<option value="linkedin" data-url="<?php echo plugin_dir_url( __FILE__ ). "images/linkedin.png"; ?>" <?php if($b['types']=="linkedin") echo "selected"; ?> > Linkedin </option>
												<option value="youtube" data-url="<?php echo plugin_dir_url( __FILE__ ). "images/youtube.png"; ?>" <?php if($b['types']=="youtube") echo "selected"; ?> > Youtube </option>
											</select>
										</td>
										<td><input type="text" value="<?php echo $b['links']; ?>" name="<?php echo 'social_media['.$i.'][links]'; ?>" data-classname="links" class="social_media_table_textbox"> </td>
										<td> <button class="remove_social_media"> Remove </button></td>
									</tr>
									<?php
									$i++;
								}
							?>
						</tbody>
					</table>
						<button id="add_social_media"> Add Row </button>
				</div>
			</div>
			<?php
		}
	}
	
	// Save custom meta box values for Team Members
	function save_custom_fields_for_memebers(){
		global $post;
		global $wpdb;
		if ( $post )
		{
			if(isset($_POST['designation']))
            {
				$designation = sanitize_text_field($_POST['designation']);
                update_post_meta($post->ID, "designation", $designation );
            }
            if(isset($_POST['addl_info']))
            {
				$addl_info = sanitize_text_field($_POST['addl_info']);
                update_post_meta($post->ID, "addl_info", $_POST["addl_info"]);
            }
            if(isset($_POST['social_media']))
            {
				$social_media = sanitize_text_field($_POST['social_media']);
                update_post_meta($post->ID, "social_media", $_POST["social_media"]);
            }
	  	}
	}
	
	// Post view in Team Members
	function custom_columns( $columns ) {
    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => 'Title',
        'featured_image' => 'Image',
        'designation' => 'Designation',
        'date' => 'Date'
     );
    return $columns;
	}

	function custom_columns_data( $column, $post_id ) {
		switch ( $column ) {
			case 'featured_image':
			the_post_thumbnail( 'thumbnail' );
			break;

			case 'designation':
			echo get_post_meta($post_id,"designation",true);
			break;
		}
	}
	
	function set_shortcode_columns_in_terms($columns) {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => 'Title',
			'shortcode' => 'Shortcode',
			'date' => 'Date'
		);
		
		return $columns;
	}

	function set_shortcode_data( $column, $post_id ) {
		switch ( $column ) {
			case 'shortcode' :
				if(get_post_meta($post_id, "shortcode", true)!="")
			{   
				?>
				<span class="shortcode_show"><?php echo get_post_meta($post_id, "shortcode", true); ?> </span>
				<span class="copy_to_clipboard_shortcode_btn">
					<button data-toggle="tooltip"  id="copy_to_clipboard_shortcode">Copy</button>
				</span>
				<span class="copied_show"> Copied </span>
				<?php
			}
		}
	}
	
	
	
	// Create a Meta Box For teams
	function create_meta_box_for_teams()                                                                            
	{
		add_meta_box("team_shortcode","Shortcode","create_meta_box_field_for_teams_shorcode","codo-teams","normal","high");
		function create_meta_box_field_for_teams_shorcode($object){  
			if(get_post_meta($object->ID, "shortcode", true)!="")
			{   
				?>
				<div class="shortcode column-shortcode">
					<span class="shortcode_title"> Shortcode : </span>
					<span class="shortcode_show"><?php echo get_post_meta($object->ID, "shortcode", true); ?> </span>
					<span class="copy_to_clipboard_shortcode_btn">
						<button data-toggle="tooltip"  id="copy_to_clipboard_shortcode">Copy</button>
					</span>
					<span class="copied_show"> Copied </span>
				</div>
				<?php
			}
		}
		add_meta_box("teams_details","Teams Details","create_meta_box_field_for_teams","codo-teams","normal","high");
		function create_meta_box_field_for_teams($object)
		{   ?>
			<div id="teams_details">
				<div class="codo_teams_member_div">
					<p class="title_name"> Members </p>
					<p class="title_desc">Select Members From the Dropdown,drag and Drop them to reorder</p>
					<div id="select_member_selectbox">
						<select name="select-member" id="select-member">
						<option> Member List </option>
						<optgroup label="Select a Member" class="select_a_member">
						<?php
						$allposts= get_posts( array('post_type'=>'codo-members','numberposts'=>-1));
						foreach($allposts as $d)
						{
							$feature_img_url = get_the_post_thumbnail_url($d->ID);
							echo "<option value='".$d->ID."' data-url='".$feature_img_url."'>".$d->post_title."</option>";
						}
						?>
						</optgroup><optgroup label="Already-Added" class="already_added"></optgroup>
						</select>
					</div>
						<?php
						$member_list=get_post_meta($object->ID, "members_list", true);
						?>
						<br>
						<input type="hidden" id="thedata" name="member" value="<?php echo get_post_meta($object->ID, "members_list", true) ?>">
							<ul id="member-list">
								<?php
									if($member_list!="")
									{
										$member_list=explode(',',$member_list);
										foreach($member_list as $id)
										{
											$post_feature_image=get_the_post_thumbnail_url($id);
											$post_title=get_the_title($id);
											echo '<li id="member_'.$id.'" data-index="'.$id.'">
											<div class="member_list_show"><img class="member_photo" src="'.$post_feature_image.'">
											</img><span class="member_title">'.$post_title.'</span>
											<a class="remove_member"></a></div></li>';
										}
									}
								?>
							</ul>
				</div>
			
			<div class="codo_teams_presets_div">
				<p class="title_name">Presets</p>
				<p class="title_desc">Choose a Preset From Below</p>
				<input type="hidden" id="choosed_presets" name="presets_type" value="<?php echo get_post_meta($object->ID, "presets_type", true) ?>">
				<div id="choose_presets_ul">
					<?php
						$dir_path=plugin_dir_path( __FILE__ )."/../public/templates/*";
						$dirs = array_filter(glob($dir_path), 'is_dir');
						foreach($dirs as $dir)
						{
							$image = glob($dir . "/preview.png");
							$child_dir=plugin_dir_path( __FILE__ )."/../public/templates/".basename($dir)."/*";
							$child_dirs = array_filter(glob($child_dir), 'is_dir');
							$style_folder=array();
							foreach($child_dirs as $a){
								$style_folder[]=basename($a);
							}
							$style_folder=json_encode($style_folder);
							if(!empty($image[0]))
							{
								?>
								<span><div data-stylefolder='<?php echo $style_folder ?>' class="codo_presets_image <?php if((get_post_meta($object->ID, "presets_type", true))==basename($dir)) { echo 'selected'; } else { echo'border_class'; } ?>" ><img class="presets_image" src="<?php echo plugin_dir_url( __FILE__ ). "../public/templates/".basename($dir)."/".basename($image[0]); ?>"><div class="codo-basename-name"><?php echo basename($dir); ?> </div></div></span>
								<?php
							}
							else
							{
								?>
								<span><div data-stylefolder='<?php echo $style_folder; ?>' class="<?php if((get_post_meta($object->ID, "presets_type", true))==basename($dir)) { echo 'selected'; } else { echo'border_class'; } ?>" ><img class="presets_image" src="<?php echo plugin_dir_url( __FILE__ ). "../public/templates/preview.png"; ?>"><br><?php echo basename($dir); ?> </div></span>
								<?php
							}
							
						}
					?>
				</div>
			</div>
			
			<div class="codo_teams_style_div">
				<p class="title_name">Styles</p>
				<p class="title_desc">We have a set of predefined styles for each preset. Choose Your Favorite, Refer demo</p>
				<div class="div_style_type">
					<?php
						$get_style_types=get_post_meta($object->ID, "presets_type", true);
						$child_style_types=plugin_dir_path( __FILE__ )."/../public/templates/".$get_style_types."/*";
						$child_style_types_array = array_filter(glob($child_style_types), 'is_dir');
					?>
						<select name="style_type" id="styles_type">
							<?php
								foreach($child_style_types_array as $value)
								{
									?>
									<option value="<?php echo basename($value); ?>" <?php if(get_post_meta($object->ID, "style_type", true)==basename($value)) echo "selected"; ?> ><?php echo basename($value); ?></option>
									<?php
								}
							?>
						</select>
				</div>
				<div class="div_columns_type">
						<select name="column_type"  id="cloumns_type" <?php if((get_post_meta($object->ID, "presets_type", true))=="list" || (get_post_meta($object->ID, "presets_type", true))=="table") { echo "disabled"; }?>>
							<option value="2" <?php if(get_post_meta($object->ID, "column_type", true)=="2") echo "selected"; ?> >Column 2</option>
							<option value="3" <?php if(get_post_meta($object->ID, "column_type", true)=="3") echo "selected"; ?> >Column 3</option>
							<option value="4" <?php if(get_post_meta($object->ID, "column_type", true)=="4") echo "selected"; ?> >Column 4</option>
						</select>
				</div>
			</div>
			<div class="codo_teams_font_size">
				<p class="title_name">Font Size</p>
				<p class="title_desc">Want to add your font size on content.Default size 16px</p>
				<input type="number" name="font_size_codo_content" id="font_size_codo_content" step="any" value="<?php echo get_post_meta($object->ID, "font_size_codo_content", true); ?>">
		   </div>
		   <div class="codo_teams_font_size">
				<p class="title_name">Font Color</p>
				<p class="title_desc">Want to add your font color on content.Default color white</p>
				<input type="text" name="font_color_codo_content" autocomplete="off" id="font_color_codo_content" step="any" value="<?php echo get_post_meta($object->ID, "font_color_codo_content", true); ?>">
		   </div>
		   <div class="codo_teams_font_size">
				<p class="title_name">Backgound Hover Color</p>
				<p class="title_desc">Want to add your backgound hover color on content.Used only in some templates</p>
				<input type="text" name="font_backgound_hover_codo_content" autocomplete="off" id="font_backgound_hover_codo_content" step="any" value="<?php echo get_post_meta($object->ID, "font_backgound_hover_codo_content", true); ?>">
		   </div>
			
		   <div class="codo_teams_custom_css_div">
				<p class="title_name">Custom CSS</p>
				<p class="title_desc">Want to add Your Own Colours and flavours? Add Your Custom CSS in the text box below</p>
				<textarea name="custom_css_textbox" rows=5 id="custom_css_input_box"><?php echo get_post_meta($object->ID, "custom_css_textbox", true); ?></textarea>
		   </div>
			
		</div>
			<?php
		}
	}
	

	// Save Custom meta box values For Temas
	function save_custom_fields_for_teams()
	{
		global $post;
		global $wpdb;
		if ( $post )
		{
			if(isset($_POST['member']))
            {
				$members_list = sanitize_text_field($_POST['member']);
                update_post_meta($post->ID, "members_list", $members_list );
            }
            if(isset($_POST['style_type']))
            {
				$style_type = sanitize_text_field( $_POST['style_type']);
                update_post_meta($post->ID, "style_type", $style_type );
            }
            if(isset($_POST['presets_type']))
            {
				$presets_type = sanitize_text_field( $_POST['presets_type']);
                update_post_meta($post->ID, "presets_type", $presets_type );
            }
            if(isset($_POST['column_type']))
            {
				$column_type = sanitize_text_field( $_POST['column_type']);
                update_post_meta($post->ID, "column_type", $column_type );
            }
            if(isset($_POST['custom_css_textbox']))
            {
				$custom_css_textbox = sanitize_text_field( $_POST['custom_css_textbox']);
                update_post_meta($post->ID, "custom_css_textbox", $custom_css_textbox );
			}
			if(isset($_POST['font_size_codo_content']) && $_POST['font_size_codo_content'] != '' )
            {
				$font_size_codo_content = sanitize_text_field( $_POST['font_size_codo_content']);
				update_post_meta($post->ID, "font_size_codo_content", $font_size_codo_content );
				
			}else{
				update_post_meta($post->ID, "font_size_codo_content", '16');
			}
			if(isset($_POST['font_color_codo_content']))
            {
				$font_color_codo_content = sanitize_text_field( $_POST['font_color_codo_content']);
				update_post_meta($post->ID, "font_color_codo_content",$font_color_codo_content );
				
			}
			if(isset($_POST['font_backgound_hover_codo_content'])){
				$font_backgound_hover_codo_content = sanitize_text_field( $_POST['font_backgound_hover_codo_content']);
				update_post_meta($post->ID, "font_backgound_hover_codo_content", $font_backgound_hover_codo_content );
			}
            $shortcode='[codoteam id="'.$post->ID.'"]';
            if(isset($shortcode))
            {
                update_post_meta($post->ID, "shortcode", $shortcode);
            }
		}
	}
	
	
	// Shortcode Create For Every Teams
	function codoteam($atts)
	{
		extract( shortcode_atts(array('id' => '',), $atts ));
		$members_list=get_post_meta($id,'members_list',true);
		$presets_type=get_post_meta($id,'presets_type',true);
		$column_type=get_post_meta($id,'column_type',true);
		$style_type=get_post_meta($id,'style_type',true);
		$custom_css_textbox=get_post_meta($id,'custom_css_textbox',true);
		$font_size_codo_content=get_post_meta($id,'font_size_codo_content',true);
		$font_color_codo_content=get_post_meta($id,'font_color_codo_content',true);
		$font_backgound_hover_codo_content=get_post_meta($id,'font_backgound_hover_codo_content',true);
		
		
		require_once('includes/scssphp/scss.inc.php');
		$scss = new scssc();
		$convert_css= '.codoteam_'.$id. ' { '.$custom_css_textbox. ' } ';
		$custom_css=$scss->compile($convert_css);
		ob_start();
		?>
		<style>
		.<?php echo 'codoteam_'.$id; ?> p {
			font-size: <?php echo($font_size_codo_content) ?>px!important;
		}
		.<?php echo 'codoteam_'.$id; ?> .codo_hover_color{
			color:<?php echo($font_color_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?> .codo_hover_background{
			background: <?php echo($font_backgound_hover_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?> .codo_hover_color:after{
			color:<?php echo($font_color_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.codo-circle.style_1 .details_container::before{
			background: <?php echo($font_backgound_hover_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.codo-circle .details_container:hover .element_personal_info_hover,.<?php echo 'codoteam_'.$id; ?>.codo-circle .details_container:hover .element_name_hover {
			color:<?php echo($font_color_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.codo-grid.style_1 .element_container:hover h3,.<?php echo 'codoteam_'.$id; ?>.codo-grid.style_1 .element_container:hover .element_category_hover,.<?php echo 'codoteam_'.$id; ?>.codo-grid.style_1 .element_container:hover a.social_icons i{
			color:<?php echo($font_color_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.codo-grid.style_1 figcaption.element_personal_info_hover_container::before{
			background: <?php echo($font_backgound_hover_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.codo-grid.style_2 .element_container:hover h3,.<?php echo 'codoteam_'.$id; ?>.codo-grid.style_2 .element_container:hover .element_category_hover,.<?php echo 'codoteam_'.$id; ?>.codo-grid.style_2 .element_container:hover a.social_icons i{
			color:<?php echo($font_color_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.codo-grid.style_2 .element_container:hover figcaption.element_personal_info_hover_container::before{
			background: <?php echo($font_backgound_hover_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.codo-grid.style_2 figcaption.element_personal_info_hover_container::before{
			background: <?php echo($font_backgound_hover_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.codo-grid.style_3 figcaption.element_personal_info_hover_container::before{
			background: <?php echo($font_backgound_hover_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.codo-grid.style_3 .element_container:hover h3,.<?php echo 'codoteam_'.$id; ?>.codo-grid.style_3 .element_container:hover .element_category_hover,.<?php echo 'codoteam_'.$id; ?>.codo-grid.style_3 .element_container:hover a.social_icons i{
			color:<?php echo($font_color_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.slidein .element_personal_info_hover_container{
			background: <?php echo($font_backgound_hover_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.codo-card.style_1 .element_container:hover .details_container::before{
			background: <?php echo($font_backgound_hover_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.codo-card.style_1 .element_container:hover .element_category_hover,.<?php echo 'codoteam_'.$id; ?>.codo-card.style_1 .element_container:hover .element_name_hover,.<?php echo 'codoteam_'.$id; ?>.codo-card.style_1 .element_container:hover p,.<?php echo 'codoteam_'.$id; ?>.codo-card.style_1 .element_container:hover i{
			color:<?php echo($font_color_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.codo-card.style_2 .element_container:hover .element_personal_info_hover_container{
			background: <?php echo($font_backgound_hover_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.codo-card.style_2 .element_container:hover .element_category_hover,.<?php echo 'codoteam_'.$id; ?>.codo-card.style_2 .element_container:hover .element_name_hover,.<?php echo 'codoteam_'.$id; ?>.codo-card.style_2 .element_container:hover p,.<?php echo 'codoteam_'.$id; ?>.codo-card.style_2 .element_container:hover i{
			color:<?php echo($font_color_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.codo-card.style_3 .element_container:hover .element_category_hover,.<?php echo 'codoteam_'.$id; ?>.codo-card.style_3 .element_container:hover .element_name_hover,.<?php echo 'codoteam_'.$id; ?>.codo-card.style_3 .element_container:hover p,.<?php echo 'codoteam_'.$id; ?>.codo-card.style_3 .element_container:hover i{
			color:<?php echo($font_color_codo_content) ?>!important;
		}
		.<?php echo 'codoteam_'.$id; ?>.codo-slidein .element_personal_info_hover_container{
			background: <?php echo($font_backgound_hover_codo_content) ?>!important;
		}
		</style>
		<?php
		if($presets_type=="Modal" && $style_type=="style1")
		{
			require(__DIR__ .'/../public/templates/Modal/style1/index.php');
		}
		else if($presets_type=="Push Out" && $style_type=="style1")
		{
			require(__DIR__ .'/../public/templates/Push Out/style1/index.php');
		}
		else if($presets_type=="Push Out" && $style_type=="style2")
		{
			require(__DIR__ .'/../public/templates/Push Out/style2/index.php');
		}
		else if($presets_type=="card" && $style_type=="style1")
		{
			require(__DIR__ .'/../public/templates/card/style1/index.php');
		}
		else if($presets_type=="card" && $style_type=="style2")
		{
			require(__DIR__ .'/../public/templates/card/style2/index.php');
		}
		else if($presets_type=="card" && $style_type=="style3")
		{
			require(__DIR__ .'/../public/templates/card/style3/index.php');
		}
		else if($presets_type=="card" && $style_type=="style4")
		{
			require(__DIR__ .'/../public/templates/card/style4/index.php');
		}
		else if($presets_type=="circle" && $style_type=="style1")
		{
			require(__DIR__ .'/../public/templates/circle/style1/index.php');
		}
		else if($presets_type=="circle" && $style_type=="style2")
		{
			require(__DIR__ .'/../public/templates/circle/style2/index.php');
		}
		else if($presets_type=="circle" && $style_type=="style3")
		{
			require(__DIR__ .'/../public/templates/circle/style3/index.php');
		}
		else if($presets_type=="circle" && $style_type=="style4")
		{
			require(__DIR__ .'/../public/templates/circle/style4/index.php');
		}
		else if($presets_type=="grid" && $style_type=="style1")
		{
			require(__DIR__ .'/../public/templates/grid/style1/index.php');
		}
		else if($presets_type=="grid" && $style_type=="style2")
		{
			require(__DIR__ .'/../public/templates/grid/style2/index.php');
		}
		else if($presets_type=="grid" && $style_type=="style3")
		{
			require(__DIR__ .'/../public/templates/grid/style3/index.php');
		}
		else if($presets_type=="grid" && $style_type=="style4")
		{
			require(__DIR__ .'/../public/templates/grid/style4/index.php');
		}
		else if($presets_type=="slidein" && $style_type=="style1")
		{
			require(__DIR__ .'/../public/templates/slidein/style1/index.php');
		}
		else if($presets_type=="slidein" && $style_type=="style2")
		{
			require(__DIR__ .'/../public/templates/slidein/style2/index.php');
		}
		else if($presets_type=="list" && $style_type=="style1")
		{
			require(__DIR__ .'/../public/templates/list/style1/index.php');
		}
		else if($presets_type=="list" && $style_type=="style2")
		{
			require(__DIR__ .'/../public/templates/list/style2/index.php');
		}
		else if($presets_type=="table" && $style_type=="style1")
		{
			require(__DIR__ .'/../public/templates/table/style1/index.php');
		}
		else if($presets_type=="table" && $style_type=="style2")
		{
			require(__DIR__ .'/../public/templates/table/style2/index.php');
		}
		else if($presets_type=="table" && $style_type=="style3")
		{
			require(__DIR__ .'/../public/templates/table/style3/index.php');
		}
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}
	public function codo_import_function(){
		$data =  file_get_contents(plugin_dir_path( __FILE__ ) .'partials/codo-import.json');
		$import_object = json_decode($data,true);
		$postid_array = array();
		foreach( $import_object['codo_members'] as $codo_members){
				$image_url = $codo_members['image'];
			
				$upload_dir = wp_upload_dir();

				$image_data = file_get_contents( plugin_dir_path( __FILE__ ) .'images/demo/'.$image_url );

				$filename = basename( $image_url );

				if ( wp_mkdir_p( $upload_dir['path'] ) ) {
				  $file = $upload_dir['path'] . '/' . $filename;
				}
				else {
				  $file = $upload_dir['basedir'] . '/' . $filename;
				}

				file_put_contents( $file, $image_data );

				$wp_filetype = wp_check_filetype( $filename, null );

				$attachment = array(
				  'post_mime_type' => $wp_filetype['type'],
				  'post_title' => sanitize_file_name( $filename ),
				  'post_content' => '',
				  'post_status' => 'inherit'
				);

				$attach_id = wp_insert_attachment( $attachment, $file );
				require_once( ABSPATH . 'wp-admin/includes/image.php' );
				$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
				wp_update_attachment_metadata( $attach_id, $attach_data );
				
			$post_arr = array(
				'post_title'   => $codo_members['post_title'],
				'post_content'   => $codo_members['post_content'],
				'post_status'  => 'publish',
				'post_type' =>'codo-members',
				'post_author'  => get_current_user_id(),
					'meta_input'   => $codo_members['meta_input']
				);
			$post_id = wp_insert_post( $post_arr);
			$postid_array[] = $post_id;
			set_post_thumbnail( $post_id, $attach_id );
				
		}
		
		foreach( $import_object['codo_teams'] as $codo_teams){
			$post_arr = array(
				'post_title'   => $codo_teams['post_title'],
				'post_status'  => 'publish',
				'post_type' =>'codo-teams',
				'post_author'  => get_current_user_id(),
					'meta_input'   => $codo_teams['meta_input']
				);
			$post_id = wp_insert_post( $post_arr);
			$members_list = implode(', ', $postid_array);
			update_post_meta($post_id,'members_list',$members_list );
			$shortcode='[codoteam id="'.$post_id.'"]';
			update_post_meta($post_id, "shortcode", $shortcode);
		}
	}
	
}
