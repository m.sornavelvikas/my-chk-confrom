<?php

/** 
 *
 * @link              www.codosome.com
 * @since             1.0.0
 * @package           Codo_Teams
 *
 * @wordpress-plugin
 * Plugin Name:       Codo Teams
 * Plugin URI:        codosome.com
 * Description:       Insert your team members as a grid or any other layout you prefer from our list of layouts.
 * Version:           1.0.0
 * Author:            Codosome
 * Author URI:        www.codosome.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       codo-teams
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'CODO_TEAMS_VERSION', '1.0.0' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-codo-teams.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_codo_teams() {

	$plugin = new Codo_Teams();
	$plugin->run();

}
run_codo_teams();
